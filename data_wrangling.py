from collections import deque
from typing import Dict, Generator, List
import pandas as pd
import glob
import os
from text_engineering import get_windowed_sentence_texts, get_tokenized_sentence_texts


def load_csvs(path: str) -> pd.DataFrame:
    all_files = glob.glob(os.path.join(path, "*.csv"))
    frames = [pd.read_csv(f) for f in all_files]
    df = pd.concat(frames, ignore_index=False)
    return df


def filter_service(df: pd.DataFrame) -> pd.DataFrame:
    relevant_services = {"Pathology", "Bone Marrow Transplant"}
    filtered = df.loc[df["Service"].isin(relevant_services)]
    return filtered


def dataset_generator(
    df: pd.DataFrame,
    character_window: bool = False
    # ) -> List[Dict[str, str]]:
) -> Generator[Dict[str, str], None, None]:
    get_instances = lambda _: []
    if character_window:
        get_instances = get_windowed_sentence_texts
    else:
        get_instances = get_tokenized_sentence_texts
    out_ls = deque()
    for _, row in df.iterrows():
        mrn = row["MRN"]
        encounter_date = row["EncounterDate"]
        service = row["Service"]
        note_text = row["Note Text"]
        for sentence in get_instances(note_text):
            # out_ls.append(
            if character_window:
                spoof = " ".join(["O"] * len(sentence))
                yield {
                    "mrn": str(mrn),
                    "encounter_date": str(encounter_date),
                    "service": str(service),
                    "text": str(sentence),
                    "codon": spoof,
                    "syntax_n": spoof,
                    "syntax_p": spoof,
                }
            else:
                spoof = " ".join(["O"] * len(sentence.split()))
                yield {
                    "mrn": str(mrn),
                    "encounter_date": str(encounter_date),
                    "service": str(service),
                    "text": str(sentence),
                    "gene": spoof,
                    "interpretation": spoof,
                    "exon": spoof,
                }
    #         )
    # return list(out_ls)


def filtered_dataset(path: str) -> pd.DataFrame:
    df = load_csvs(path)
    filtered_df = filter_service(df)
    return filtered_df


def dataset_list_from_path(
    df: pd.DataFrame, character_window: bool
) -> List[Dict[str, str]]:
    return list(dataset_generator(df, character_window))
