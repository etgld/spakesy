from collections import defaultdict
from datasets import Dataset
import pandas as pd
import csv
from data_wrangling import dataset_list_from_path, filtered_dataset
from transformers import (
    AutoConfig,
    AutoModel,
    AutoTokenizer,
    EvalPrediction,
    HfArgumentParser,
    Trainer,
    TrainerCallback,
    set_seed,
)
from cnlpt.CnlpModelForClassification import CnlpConfig, CnlpModelForClassification
from cnlpt.cnlp_predict import (
    process_prediction,
    restructure_prediction,
    structure_labels,
)
from cnlpt.cnlp_data import cnlp_preprocess_data

AutoConfig.register("cnlpt", CnlpConfig)


def driver(
    codon_syntaxes_model_path: str, gene_int_exon_model_path: str, csv_path: str
):
    AutoModel.register(CnlpConfig, CnlpModelForClassification)

    codon_syntaxes_config = AutoConfig.from_pretrained(
        codon_syntaxes_model_path,
        character_level=True,
    )

    gene_int_exon_config = AutoConfig.from_pretrained(
        gene_int_exon_model_path,
        character_level=False,
    )

    gie_tokenizer = AutoTokenizer.from_pretrained(gene_int_exon_model_path)

    csnp_tokenizer = AutoTokenizer.from_pretrained(codon_syntaxes_model_path)

    gie_model = CnlpModelForClassification.from_pretrained(
        gene_int_exon_model_path,
        config=gene_int_exon_config,
    )
    csnp_model = CnlpModelForClassification.from_pretrained(
        codon_syntaxes_model_path,
        config=codon_syntaxes_config,
    )

    gie_trainer = Trainer(model=gie_model)

    csnp_trainer = Trainer(model=csnp_model)
    # character_dataset = Dataset.from_list(
    #     dataset_list_from_path(csv_path, character_window=True)
    # )
    # token_dataset = Dataset.from_list(
    #     dataset_list_from_path(csv_path, character_window=False)
    # )
    # Because we're using the same version of Datasets as Australopithecus
    original_dataset = filtered_dataset(csv_path)
    character_ls = dataset_list_from_path(original_dataset, character_window=True)
    token_ls = dataset_list_from_path(original_dataset, character_window=False)
    character_dict = {
        key: [packet[key] for packet in character_ls] for key in character_ls[0].keys()
    }
    token_dict = {
        key: [packet[key] for packet in token_ls] for key in token_ls[0].keys()
    }
    raw_character_dataset = Dataset.from_dict(character_dict)
    raw_token_dataset = Dataset.from_dict(token_dict)
    character_dataset = raw_character_dataset.map(
        cnlp_preprocess_data,
        batched=True,
        batch_size=128,
        fn_kwargs={
            "tokenizer": csnp_tokenizer,
            "max_length": 128,
            "character_level": True,
            "inference": False,
            "tasks": codon_syntaxes_config.finetuning_task,
            "output_modes": defaultdict(lambda: "tagging"),
            "label_lists": codon_syntaxes_config.label_dictionary,
        },
    )
    token_dataset = raw_token_dataset.map(
        cnlp_preprocess_data,
        batched=True,
        batch_size=128,
        fn_kwargs={
            "tokenizer": gie_tokenizer,
            "max_length": 512,
            "character_level": False,
            "inference": False,
            "tasks": gene_int_exon_config.finetuning_task,
            "output_modes": defaultdict(lambda: "tagging"),
            "label_lists": gene_int_exon_config.label_dictionary,
        },
    )
    raw_tokens_prediction = gie_trainer.predict(token_dataset)
    # raw_characters_prediction = csnp_trainer.predict(character_dataset)

    (
        token_task_to_label_packet,
        token_task_to_label_boundaries,
    ) = restructure_prediction(
        gene_int_exon_config.finetuning_task,
        raw_tokens_prediction,
        512,
        defaultdict(lambda: True),
        defaultdict(lambda: False),
        False,
    )

    # (
    #     character_task_to_label_packet,
    #     character_task_to_label_boundaries,
    # ) = restructure_prediction(
    #     codon_syntaxes_config.finetuning_task,
    #     raw_characters_prediction,
    #     128,
    #     defaultdict(lambda: True),
    #     defaultdict(lambda: False),
    #     False,
    # )
    token_out_table = process_prediction(
        gene_int_exon_config.finetuning_task,
        False,
        False,
        False,
        token_task_to_label_packet,
        token_task_to_label_boundaries,
        token_dataset,
        gene_int_exon_config.label_dictionary,
        defaultdict(lambda: "tagging"),
    )

    # character_out_table = process_prediction(
    #     codon_syntaxes_config.finetuning_task,
    #     False,
    #     False,
    #     True,
    #     character_task_to_label_packet,
    #     character_task_to_label_boundaries,
    #     character_dataset,
    #     codon_syntaxes_config.label_dictionary,
    #     defaultdict(lambda: "tagging"),
    # )

    token_df = pd.merge(
        token_dataset.to_pandas().drop(
            [
                "input_ids",
                "attention_mask",
                "word_ids",
                "label",
                "event_mask",
                "text",
                "gene",
                "interpretation",
                "exon",
            ],
            axis=1,
        ),
        token_out_table,
        left_index=True,
        right_index=True,
    )

    token_with_signal = token_df[
        (token_df["gene"].str.len() > 0)
        | (token_df["interpretation"].str.len() > 0)
        | (token_df["exon"].str.len() > 0)
    ]

    # character_df = pd.merge(
    #     character_dataset.to_pandas().drop(
    #         ["text", "codon", "syntax_n", "syntax_p"], axis=1
    #     ),
    #     character_out_table,
    #     left_index=True,
    #     right_index=True,
    # )
    # character_with_signal = character_df[
    #     (character_df["codon"].str.len() > 0)
    #     | (character_df["syntax_n"].str.len() > 0)
    #     | (character_df["syntax_p"].str.len() > 0)
    # ]

    print(token_with_signal)
    token_with_signal.to_csv(
        f"genes.tsv",
        sep="\t",
        index=True,
        header=True,
        quoting=csv.QUOTE_NONE,
        escapechar="\\",
    )
    # print(character_with_signal)


def main():
    driver(
        "/home/etg/clingen_joint_models/codon_syntaxes_canine/",
        "/home/etg/clingen_joint_models/gie_biomed_roberta/",
        "/home/etg/first_eight_neutropenia",
    )


if __name__ == "__main__":
    main()
