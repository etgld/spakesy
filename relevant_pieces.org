
Model loading logic from on disk checkpoint

#+BEGIN_SRC
    else:
        # by default cnlpt model, but need to check which encoder they want
        encoder_name = model_args.encoder_name

        # TODO check when download any pretrained language model to local disk, if
        # the following condition "is_hub_model(encoder_name)" works or not.
        if not is_external_encoder(encoder_name):
            # we are loading one of our own trained models as a starting point.
            #
            # 1) if training_args.do_train is true:
            # sometimes we may want to use an encoder that has had continued pre-training, either on
            # in-domain MLM or another task we think might be useful. In that case our encoder will just
            # be a link to a directory. If the encoder-name is not recognized as a pre-trained model, special
            # logic for ad hoc encoders follows:
            # we will load it as-is initially, then delete its classifier head, save the encoder
            # as a temp file, and make that temp file
            # the model file to be loaded down below the normal way. since that temp file
            # doesn't have a stored classifier it will use the randomly-inited classifier head
            # with the size of the supplied config (for the new task).
            # TODO This setting 1) is not tested yet.
            # 2) if training_args.do_train is false:
            # we evaluate or make predictions of our trained models.
            # Both two setting require the registeration of CnlpConfig, and use
            # AutoConfig.from_pretrained() to load the configuration file
            AutoModel.register(CnlpConfig, CnlpModelForClassification)

            # Load the cnlp configuration using AutoConfig, this will not override
            # the arguments from trained cnlp models. While using CnlpConfig will override
            # the model_type and model_name of the encoder.
            config = AutoConfig.from_pretrained(
                model_args.config_name
                if model_args.config_name
                else model_args.encoder_name,
                cache_dir=model_args.cache_dir,
                # in this case we're looking at a fine-tuned model (?)
                character_level=data_args.character_level,
            )

            if training_args.do_train:
                # Setting 1) only load weights from the encoder
                raise NotImplementedError(
                    "This functionality has not been restored yet"
                )
                model = CnlpModelForClassification(
                    model_path=model_args.encoder_name,
                    config=config,
                    cache_dir=model_args.cache_dir,
                    tagger=tagger,
                    relations=relations,
                    class_weights=dataset.class_weights,
                    final_task_weight=training_args.final_task_weight,
                    use_prior_tasks=model_args.use_prior_tasks,
                    argument_regularization=model_args.arg_reg,
                )
                delattr(model, "classifiers")
                delattr(model, "feature_extractors")
                if training_args.do_train:
                    tempmodel = tempfile.NamedTemporaryFile(dir=model_args.cache_dir)
                    torch.save(model.state_dict(), tempmodel)
                    model_name = tempmodel.name
            else:
                # setting 2) evaluate or make predictions
                model = CnlpModelForClassification.from_pretrained(
                    model_args.encoder_name,
                    config=config,
                    class_weights=dataset.class_weights,
                    final_task_weight=training_args.final_task_weight,
                    freeze=training_args.freeze,
                    bias_fit=training_args.bias_fit,
                )

        else:
            # This only works when model_args.encoder_name is one of the
            # model card from https://huggingface.co/models
            # By default, we use model card as the starting point to fine-tune
            encoder_name = (
                model_args.config_name
                if model_args.config_name
                else model_args.encoder_name
            )
            config = CnlpConfig(
                encoder_name=encoder_name,
                finetuning_task=data_args.task_name
                if data_args.task_name is not None
                else dataset.tasks,
                layer=model_args.layer,
                tokens=model_args.token,
                num_rel_attention_heads=model_args.num_rel_feats,
                rel_attention_head_dims=model_args.head_features,
                tagger=tagger,
                relations=relations,
                label_dictionary=dataset.get_labels(),
                character_level=data_args.character_level,
                # num_tokens=len(tokenizer),
            )
            config.vocab_size = len(tokenizer)
            model = CnlpModelForClassification(
                config=config,
                class_weights=dataset.class_weights,
                final_task_weight=training_args.final_task_weight,
                freeze=training_args.freeze,
                bias_fit=training_args.bias_fit,
            )
#+END_SRC

When ~trainer.predict~ is called on a segment of ~ClinicalNlpDataset~,
the segment is actually a normal Huggingface Dataset as returned by
~get_dataset_segment~
#+BEGIN_SRC 
def get_dataset_segment(
    split_name: str,
    dataset_ind: int,
    dataset: ClinicalNlpDataset,
):
    start_ind = end_ind = 0
    for ind in range(dataset_ind):
        start_ind += len(dataset.datasets[ind][split_name])
    end_ind = start_ind + len(dataset.datasets[dataset_ind][split_name])

    return HFDataset.from_dict(dataset.processed_dataset[split_name][start_ind:end_ind])
#+END_SRC

Then ~restructure_prediction~ is called on the result of ~trainer.predict~
