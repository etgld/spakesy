import requests


def ctakes_process(sent):
    url = "http://localhost:4000/ctakes-web-rest/service/analyze"
    # url = "http://ctakes:8080/ctakes-web-rest/service/analyze"
    r = requests.post(url, data=sent.encode("utf-8"), params={"format": "full"})
    return r.json()
