FROM eclipse-temurin:8-jdk as java_build

# DE HAHVAHD PRAHXY
ENV http_proxy=http://proxy.tch.harvard.edu:3128
RUN apt-get update && apt-get install -y ca-certificates openssl wget unzip git-core maven libnss3

## Download apache-tomcat and extract:
RUN wget -q https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.21/bin/apache-tomcat-9.0.21.zip
RUN unzip apache-tomcat-9.0.21.zip

## Check out version of ctakes with best working web-rest module
## Then compile with maven
RUN git clone https://github.com/apache/ctakes.git
WORKDIR /ctakes/
RUN git checkout HEAD~2
WORKDIR /
COPY Default.piper /ctakes/ctakes-web-rest/src/main/resources/pipers/TinyRestPipeline.piper
COPY Default.piper /ctakes/ctakes-web-rest/src/main/resources/pipers/Default.piper

WORKDIR /ctakes/ctakes-web-rest/
RUN mvn compile -DskipTests
RUN mvn install -DskipTests

FROM tomcat:9.0-jre8-temurin
COPY --from=java_build /ctakes/ctakes-web-rest/target/ctakes-web-rest.war $CATALINA_HOME/webapps/
ENV CTAKES_HOME=/ctakes
CMD ["catalina.sh", "run"]
