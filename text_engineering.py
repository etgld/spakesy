from collections import deque
from typing import Dict, Iterable, List, Union

from more_itertools import flatten

from ctakes_rest import ctakes_process

token_keys = {
    "WordToken",
    "PunctuationToken",
    "SymbolToken",
    # Ignoring these
    # 'NewlineToken',
    "NumToken",
    "ContractionToken",
}

annotation_keys = {
    "DateAnnotation",
    "FractionAnnotation",
    "MeasurementAnnotation",
    "PersonTitleAnnotation",
    "TimeAnnotation",
    "RangeAnnotation",
    "RomanNumeralAnnotation",
}


def token_to_stanza(index, ctakes_token, sent_text, sent_begin):
    begin = ctakes_token["begin"]
    end = ctakes_token["end"]
    token_text = sent_text[begin:end]
    return {
        "id": index + 1,
        "token_text": token_text,
        "start_char": begin + sent_begin,
        "end_char": end + sent_begin,
    }


def process_sentence(sentence):
    begin = sentence["begin"]
    sent_text = sentence["text"]
    relevant_view = ctakes_process(sent_text)["_views"]["_InitialView"]
    raw_tokens = sorted(
        flatten(relevant_view.get(t_key, []) for t_key in token_keys),
        key=lambda s: (s["begin"], s["end"]),
    )
    if len(raw_tokens) == 0:
        print("Something wrong")
        print(sent_text)
        print(relevant_view)

    raw_annotations = sorted(
        flatten(relevant_view.get(a_key, []) for a_key in annotation_keys),
        key=lambda s: (s["begin"], s["end"]),
    )

    prime_annotations = []

    for i in range(0, len(raw_annotations)):
        prev = {"begin": -1, "end": -1} if i == 0 else prime_annotations[-1]
        curr = raw_annotations[i]
        if (prev["begin"] >= curr["begin"]) and (curr["end"] >= prev["end"]):
            prime_annotations = [*prime_annotations[:-1], curr]
        elif prev["end"] <= curr["begin"]:
            prime_annotations = [*prime_annotations, curr]

    annotations_and_tokens = sorted(
        flatten((prime_annotations, raw_tokens)), key=lambda s: (s["begin"], s["end"])
    )

    final_spans = []

    for i in range(0, len(annotations_and_tokens)):
        prev = {"begin": -1, "end": -1} if i == 0 else final_spans[-1]
        curr = annotations_and_tokens[i]
        if (prev["begin"] >= curr["begin"]) and (curr["end"] >= prev["end"]):
            # remove prev
            final_spans = [*final_spans[:-1], curr]
        elif prev["end"] <= curr["begin"]:
            final_spans = [*final_spans, curr]

    def local_stanza(index, ctakes_token):
        return token_to_stanza(index, ctakes_token, sent_text, begin)

    return [
        local_stanza(index, ctakes_token)
        for index, ctakes_token in enumerate(
            sorted(final_spans, key=lambda s: s["begin"])
        )
    ]


def get_raw_sentences(text: str) -> List[Dict[str, Union[int, str]]]:
    relevant_view = ctakes_process(text)["_views"]["_InitialView"]
    ctakes_sentences = relevant_view["Sentence"]

    def basic_dict(sent):
        begin = sent["begin"]
        end = sent["end"]
        return {"begin": begin, "end": end, "text": text[begin:end]}

    return sorted(
        (basic_dict(sent) for sent in ctakes_sentences), key=lambda s: s["begin"]
    )


def get_token_aware_windows(
    sentence: Dict[str, Union[int, str]],
    max_length: int = 128,
    overlap_size: int = 0,
) -> Iterable[Dict[str, Union[int, str]]]:
    windows = deque()
    sent_begin = int(sentence["begin"])
    sent_end = int(sentence["end"])
    remaining_characters = str(sentence["text"])
    if sent_end - sent_begin <= max_length:
        windows.append(sentence)
        return windows
    cas_window_start = sent_begin
    local_window_start = 0
    in_token = False
    in_token_begin = -1
    in_token_end = None
    look_behind_in_token_begin = -1
    look_behind_in_token_end = -1
    total_characters = len(remaining_characters)
    for i in range(0, total_characters):
        forward_boundary = in_token_begin - 1 if in_token else i
        if not in_token and not remaining_characters[i].isspace():
            in_token = True
            in_token_begin = i
            if (
                i >= (local_window_start + max_length) - overlap_size
                and look_behind_in_token_begin == -1
            ):
                look_behind_in_token_begin = in_token_begin
        if in_token and remaining_characters[i].isspace():
            in_token = False
            in_token_end = i
            if (
                i >= (local_window_start + max_length) - overlap_size
                and look_behind_in_token_end == -1
            ):
                look_behind_in_token_end = in_token_end
        if i == total_characters - 1:
            this_begin = cas_window_start
            this_end = sent_end
            windows.append(
                {
                    "begin": this_begin,
                    "end": this_end,
                    "text": str(sentence["text"])[this_begin:this_end],
                }
            )
        elif i == local_window_start + max_length:
            this_begin = cas_window_start
            this_end = this_begin + forward_boundary
            windows.append(
                {
                    "begin": this_begin,
                    "end": this_end,
                    "text": str(sentence["text"])[this_begin:this_end],
                }
            )
            final_delta = (
                forward_boundary
                if overlap_size == 0
                else min(look_behind_in_token_begin, look_behind_in_token_end)
            )
            cas_window_start += final_delta
            local_window_start = final_delta

            if cas_window_start >= sent_end:
                break

    return windows


def get_windowed_sentences(
    raw_sentences: List[Dict[str, Union[int, str]]]
) -> List[Dict[str, Union[int, str]]]:
    return list(flatten(get_token_aware_windows(sent) for sent in raw_sentences))


def get_tokenized_sentences(
    raw_sentences: List[Dict[str, Union[int, str]]]
) -> List[List[Dict[str, Union[int, str]]]]:
    return [process_sentence(sent) for sent in raw_sentences]

def get_tokenized_sentence_texts(text: str) -> Iterable[str]:
    raw_sentences = get_raw_sentences(text)
    tokenized_sentence_objs = get_tokenized_sentences(raw_sentences)
    def to_str(ts_obj: List[Dict[str, Union[int, str]]]) -> str:
        return " ".join(str(token["token_text"]) for token in ts_obj)
    for ts_obj in tokenized_sentence_objs:
        yield to_str(ts_obj)

def get_windowed_sentence_texts(text: str) -> Iterable[str]:
    raw_sentences = get_raw_sentences(text)
    sentence_window_objs = get_windowed_sentences(raw_sentences)
    for sent_window_obj in sentence_window_objs:
        yield str(sent_window_obj["text"])
